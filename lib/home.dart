import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task_indus/employee.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  bool _isLoggedIn = false;
  String email, password;
  bool _showPassword = false;
  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // title: Text("My title"),
      content: Text("Enter valid login credentials"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void functionCall() async {
    var url = Uri.encodeFull("http://103.252.117.204:90/Indusssp/service.asmx");
    var envelope = '''
<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <IndusMobileUserLogin1 xmlns="http://tempuri.org/">
      <UserName>2700068</UserName>
      <UserPassword>1</UserPassword>
    </IndusMobileUserLogin1>
  </soap12:Body>
</soap12:Envelope>

''';

    http.Response response = await http.post(url,
        headers: {
          "Content-Type": "text/xml; charset=utf-8",
          "SOAPAction": "http://tempuri.org/IndusMobileUserLogin1",
          // "Host": "103.252.117.204:90"
          //"Accept": "text/xml"
        },
        body: envelope);

    print(response);
    if (email == "2700068" && password == "1") {
      print("success");
      Navigator.push(
          context, new MaterialPageRoute(builder: (context) => new Employee()));
    } else if (email == null && password == null) {
      showAlertDialog(context);
      print("empty");
    } else if (email != "2700068" && password != "1") {
      showAlertDialog(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.blueAccent, //change your color here
        ),
        leading: Icon(Icons.keyboard_arrow_left),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
            child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Container(
              width: 50,
              padding: EdgeInsets.only(bottom: 30),
              child: Image.asset('Assets/youtube.png'),
            ),
            Center(
                child: Text(
              'Welcome to Indus',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
            )),
            SizedBox(
              height: 20,
              child: Center(
                child: Text(
                  'Kindly Login to continue using our app',
                  style: TextStyle(fontSize: 13, color: Colors.grey[600]),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 40, left: 40, right: 40),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Email Field must not be empty';
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                        labelText: 'Email',
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 25.0, horizontal: 10.0),
                      ),
                      textInputAction: TextInputAction.next,
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      }, // move focus to next
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Password Field must not be empty';
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                        labelText: 'Password',
                        border: OutlineInputBorder(),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 25.0, horizontal: 10.0),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            _togglevisibility();
                          },
                          child: Container(
                            height: 50,
                            width: 70,
                            padding: EdgeInsets.symmetric(vertical: 13),
                            child: Center(
                              child: Text(
                                _showPassword ? "Hide" : "Show",
                                style: TextStyle(
                                    color: Colors.blueAccent,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ),
                      textInputAction: TextInputAction.next,
                      obscureText: !_showPassword,
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      }, // move focus to next
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30)),
                      child: FlatButton(
                        minWidth: MediaQuery.of(context).size.width,
                        height: 70,
                        child: Text('Login'),
                        color: Colors.blueAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        textColor: Colors.white,
                        onPressed: () {
                          functionCall();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        )),
      ),
    );
  }
}
