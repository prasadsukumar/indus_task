import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dio/dio.dart';

class Employee extends StatefulWidget {
  @override
  _EmployeeState createState() => _EmployeeState();
}

class _EmployeeState extends State<Employee> {
  List countries = [];

  bool isSearching = false;

  getCountries() async {
    var response =
        await Dio().get('https://dummy.restapiexample.com/api/v1/employees');
    print(response.data);
    print(response.data['data']);
    return response.data['data'];
  }

  @override
  void initState() {
    getCountries().then((data) {
      setState(() {
        countries = data;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.blueAccent, //change your color here
        ),
        leading: Icon(Icons.keyboard_arrow_left),
        backgroundColor: Colors.white,
        title: Text(
          'Employee Details',
          style: TextStyle(color: Colors.blueAccent),
        ),
      ),
      // appBar: AppBar(
      //     backgroundColor: Colors.blueGrey.shade900,
      //     title: Text('All Countries')),
      body: Container(
        padding: EdgeInsets.all(10),
        child: countries.length > 0
            ? ListView.builder(
                itemCount: countries.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation: 1.5,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ListTile(
                        title: Row(
                          children: <Widget>[
                            // Container(
                            //   width: 60,
                            //   height: 60,
                            //   decoration: BoxDecoration(
                            //       borderRadius: BorderRadius.circular(60 / 2),
                            //       image: DecorationImage(
                            //           fit: BoxFit.cover,
                            //           image: NetworkImage(
                            //               "https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.nicepng.com%2Fpng%2Fdetail%2F136-1366211_group-of-10-guys-login-user-icon-png.png&imgrefurl=https%3A%2F%2Fwww.nicepng.com%2Fourpic%2Fu2q8i1t4t4t4q8a9_group-of-10-guys-login-user-icon-png%2F&tbnid=gHyezIyavOkvwM&vet=12ahUKEwjN2u6ExbbwAhXmtUsFHQ9TADgQMygTegUIARD6AQ..i&docid=9FF7Wj-v_9JOdM&w=820&h=480&q=user&ved=2ahUKEwjN2u6ExbbwAhXmtUsFHQ9TADgQMygTegUIARD6AQ"))),
                            // ),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width - 140,
                                    child: Text(
                                      "Emp Id :" +
                                          countries[index]['id'].toString(),
                                      style: TextStyle(fontSize: 17),
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  countries[index]['employee_name'],
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  countries[index]['employee_age'].toString() +
                                      "yrs",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Rs:" +
                                      countries[index]['employee_salary']
                                          .toString(),
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                  // return GestureDetector(
                  //   onTap: () {},
                  //   child: Card(
                  //     elevation: 5,
                  //     child: Padding(
                  //       padding: const EdgeInsets.symmetric(
                  //           vertical: 10, horizontal: 8),
                  //       child: Row(
                  //         children: [
                  //           Text(
                  //             countries[index]['id'].toString() + ':',
                  //             style: TextStyle(fontSize: 18),
                  //           ),
                  //           SizedBox(),
                  //           Text(
                  //             countries[index]['employee_name'],
                  //             style: TextStyle(fontSize: 18),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // );
                })
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }
}
